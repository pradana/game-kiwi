import 'package:dio/dio.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';
import 'package:shared_preferences/shared_preferences.dart';

//String ip = "https://api.pikap.id/api";
String ip = "https://psg.pw";
//String ip = "http://192.168.137.1:8002";
//String ip = "http://192.168.100.12:8002";
String pathCoverImages = ip+"/images/cover/";
//String ip = "http://192.168.43.224:8002";
//String ip = "http://10.200.54.52:8000";
String token;

Future postLogin(String token) async {
  try {
    String path = "$ip/api-login";
    Response response = await Dio().post(path, data: {"gtoken": token});
    print(response);
    return response.data;
  } catch (e) {
    print("error code: " + e.toString());
    return null;
  }
}

class GenRequest {

  getApi(portal) async {
    try {
      var token = await getPrefferenceToken();

      Response response = await Dio().get(ip + "/api/" + portal,
          options: Options(headers: {'GENOSSYS-X': 'GENOSSYS '+token}));
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return null;
    }
  }
  getApiPublic(portal) async {
    try {
      Response response = await Dio().get(ip + "/api-public/" + portal);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return null;
    }
  }
  getApiUsingParams(portal, params) async {
    try {
      var token = await getPrefferenceToken();
      Response response = await Dio().get(ip + "/api/" + portal, queryParameters: params,
          options: Options(headers: {'GENOSSYS-X': 'GENOSSYS '+token}));
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return null;
    }
  }
  getFreeApi(portal) async {
    try {
      Response response = await Dio().get(portal);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());
      return null;
    }
  }

  postApiPublic(portal, Map<dynamic, dynamic> data) async {
    try {
      Response response = await Dio().post(ip + "/" + portal,
        data: data,);
      print(portal);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());

      return 500;
    }
  }
  postApi(portal, Map<dynamic, dynamic> data) async {
    try {
      var token = await getPrefferenceToken();

      Response response = await Dio().post(ip + "/api/" + portal,
          data: data,
          options: Options(headers: {'GENOSSYS-X': 'GENOSSYS '+token}));
      print(portal);
      return response.data;
    } catch (e) {
      print("error code: " + e.toString());

      return 500;
    }
  }

  deleteApi(portal) async {
    var token = await getPrefferenceToken();

    try {
      Response response = await Dio().delete(ip + "/api/" + portal,
          options: Options(headers: {'GENOSSYS-X': 'GENOSSYS '+token}));
      return response.data;
    } catch (e) {
      return 500;
    }
  }

}
