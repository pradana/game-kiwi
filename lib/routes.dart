
import 'package:kiwi_game/page/dashboard/coin.dart';
import 'package:kiwi_game/page/dashboard/leaderBoard.dart';
import 'package:kiwi_game/page/dashboard/notification.dart';
import 'package:kiwi_game/page/login/loginPage.dart';
import 'package:kiwi_game/page/quizes/ChoosePackagePage.dart';
import 'package:kiwi_game/page/quizes/chooseLevelPage.dart';
import 'package:kiwi_game/page/quizes/finishQuiz.dart';
import 'package:kiwi_game/page/quizes/playQuiz.dart';
import 'package:kiwi_game/page/splashScreenPage.dart';
import 'package:provider/provider.dart';
import 'blocs/baseBloc.dart';
import 'page/dashboard/Dashboard.dart';

class GenProvider {
  static var providers = [
    ChangeNotifierProvider<BaseBloc>.value(value: BaseBloc())
  ];

  static routes(context) {
    return {
      '/': (context) {
        return Dashboard();
      },

      'splashScreen': (context) {
        return SplashScreenPage();
      },

      'loginPage': (context) {
        return LoginPage();
      },

      'chooseLevelPage': (context) {
        return ChooseLevelPage();
      },

      'choosePackagePage': (context) {
        return ChoosePackagePage();
      },

      'leaderBoard': (context) {
        return LeaderBoard();
      },

      'coin': (context) {
        return Coin();
      },

      'notification': (context) {
        return NotificationPage();
      },
      ChoosePackagePageState.routeName: (context) => ChoosePackagePage(),
      PlayQuizState.routeName: (context) => PlayQuiz(),
      FinishQuizState.routeName: (context) => FinishQuiz(),

    };
  }
}