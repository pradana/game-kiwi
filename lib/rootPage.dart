import 'package:flutter/material.dart';
import 'package:kiwi_game/routes.dart';
import 'package:provider/provider.dart';


class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {



  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      child: MaterialApp(
          theme: ThemeData(fontFamily: 'Nunito'),
          initialRoute: 'splashScreen',
          routes: GenProvider.routes(context)),
      providers: GenProvider.providers,
    );
  }
}
