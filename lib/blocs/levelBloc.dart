import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class LevelBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _level = PublishSubject();

  Stream get level => _level.asBroadcastStream();

  getAllLevel()async{
    getPrefferenceLevel().then((e) async {
      dynamic data = await req.getApi("level");
      print("the level");
      print(data);
      if(data["code"] == 200){
        _level.add({ "level" : e, "dataLevel": data["payload"]});
      }else{
        _level.add([{"error" : true}]);
      }
    });

  }
}
