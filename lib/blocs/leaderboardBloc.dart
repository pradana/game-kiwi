import 'dart:async';


import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class LeaderboardBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _leaderboard = PublishSubject();

  Stream get leaderboard => _leaderboard.asBroadcastStream();

  getLeaderboard() async {
    dynamic data =
        await req.getApi("leaderboard");
    print("the leaderboard---------------------");
    print(data);
    if (data["code"] == 200) {
      _leaderboard.add(data["payload"]);
    } else {
      _leaderboard.add([
        {"error": true}
      ]);
    }
  }
}
