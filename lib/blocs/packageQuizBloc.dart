import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class PackageQuizBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _packageQuiz = PublishSubject();

  Stream get packageQuiz => _packageQuiz.asBroadcastStream();

  getPackageQuiz(level)async{
    dynamic data = await req.getApi("package/$level");
    print("the quiz package");
    print(data);
    if(data["code"] == 200){
      _packageQuiz.add(data["payload"]);
    }else{
      _packageQuiz.add([{"error" : true}]);
    }
  }
}
