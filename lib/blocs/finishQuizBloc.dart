import 'dart:async';


import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class FinishBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _finish = PublishSubject();

  Stream get finish => _finish.asBroadcastStream();

  getFinishPage(package) async {
    dynamic data =
        await req.getApiUsingParams("report-package", {"package": package});
    print("the finish---------------------");
    print(data);
    if (data["code"] == 200) {
      dynamic a = await req.postApi("quiz/submit-high-score", {"score": data["payload"][0]["exp"], "packageId": package});

      print("A IS _---------------------------------");
      print(a);
      _finish.add(data["payload"]);
    } else {
      _finish.add([
        {"error": true}
      ]);
    }
  }
}
