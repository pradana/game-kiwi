import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class DashboardBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _nickName = PublishSubject();
  final _avatar = PublishSubject();
  final _report = PublishSubject();

  Stream get nickName => _nickName.asBroadcastStream();

  Stream get avatar => _avatar.asBroadcastStream();

  Stream get report => _report.asBroadcastStream();

  getHomeData() async {
    getPrefferenceNama().then((value) {
      if (value != null) {
        _nickName.add(value);
      }
    });

    getPrefferenceAvatar().then((e) {
      if (e != null) {
        _avatar.add(e);
      }
    });

    dynamic report = await req.getApi("report");
    print("report -----");
    print(report);
    if (report["code"] == 200) {
      await setPrefferenceLevel(int.parse(report["payload"]["level"]));
      _report.add(report["payload"]);
    } else {
      _report.add({"error": "1"});
    }
  }
}
