

import 'package:flutter/material.dart';
import 'package:kiwi_game/blocs/packageQuizBloc.dart';
import 'package:kiwi_game/blocs/playQuizBloc.dart';

import 'dashboardBloc.dart';
import 'finishQuizBloc.dart';
import 'leaderboardBloc.dart';
import 'levelBloc.dart';


class BaseBloc extends ChangeNotifier {

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  DashboardBloc dashboardBloc = DashboardBloc();
  DashboardBloc get dashboard => dashboardBloc;

  LevelBloc levelBloc = LevelBloc();
  LevelBloc get level => levelBloc;

  PackageQuizBloc packageBloc = PackageQuizBloc();
  PackageQuizBloc get package => packageBloc;

  PlayQuizPackageBloc playBloc = PlayQuizPackageBloc();
  PlayQuizPackageBloc get play => playBloc;

  FinishBloc finishBloc = FinishBloc();
  FinishBloc get finish => finishBloc;

  LeaderboardBloc leaderBoardBloc = LeaderboardBloc();
  LeaderboardBloc get leaderboard => leaderBoardBloc;
}
