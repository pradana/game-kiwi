import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:rxdart/subjects.dart';
import 'package:rxdart/rxdart.dart';

class PlayQuizPackageBloc extends ChangeNotifier {
  final req = new GenRequest();

  final _playQuizBloc = PublishSubject();

  Stream get playQuizBloc => _playQuizBloc.asBroadcastStream();

  getPlayQuiz(package)async{
    dynamic data = await req.getApiUsingParams("quiz", {"package":package});
    print("the play quiz package");
    print(data);
    if(data["code"] == 200){
      _playQuizBloc.add(data["payload"]);
    }else{
      _playQuizBloc.add([{"error" : true}]);
    }
  }
}
