
import 'package:flutter/material.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';

class GenCardButton extends StatelessWidget {
  final double width;
  final double height;
  final String label;
  final double fontSize;
  final double imageSize;
  final double textheight;
  final double radius;
  final Widget child;
  final Color backGroundColor;
  final Color shadowColor;
  final Color fontColor;
  final double shadowOffset;
  final double shadowRadius;
  final EdgeInsets margin;
  final bool usingWhiteShadow;
  final Function ontap;

  GenCardButton({@required this.width,
    @required this.height,
    this.label,
    this.textheight = 20,
    this.fontSize = 12,
    this.imageSize = 0.9,
    this.child,
    this.backGroundColor = GenColor.primaryColor,
    this.shadowColor,
    this.shadowOffset,
    this.usingWhiteShadow = true,
    this.fontColor = Colors.black,
    this.radius = 10,
    this.margin = const EdgeInsets.all(0),
    this.ontap,
    this.shadowRadius});

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: margin,
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: backGroundColor,
        borderRadius: BorderRadius.circular(radius)
      ),
      child: InkWell(
        onTap: ontap != null ? ontap : (){},
        splashColor: GenColor.starColor,
        borderRadius: BorderRadius.circular(radius),
        highlightColor: Colors.white70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                child: FractionallySizedBox(
                  widthFactor: imageSize,
                  heightFactor: imageSize,
                  child: Align(alignment: Alignment.bottomCenter, child: child),
                ),
              ),
            ),
            label == null
                ? Container()
                : Container(
              height: textheight,
              child: Center(
                  child: GenText(
                    label,
                    style: TextStyle(fontSize: fontSize, color: fontColor),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
