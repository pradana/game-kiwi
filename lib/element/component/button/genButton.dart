import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';

class GenButton extends StatelessWidget {
  final Function onPressed;
  final double width;
  final double height;
  final double radius;
  final Color color;
  final String text;
  final FontWeight fontWeight;
  final double fontSize;
  final Color fontColor;
  final EdgeInsets padding;
  final bool isBorder;
  final Widget child;

  GenButton(this.text,
      {this.onPressed,
      this.width = 100,
      this.height = 50,
      this.radius = 10,
      this.fontWeight = FontWeight.bold,
      this.fontSize = 12,
      this.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      this.fontColor = Colors.white,
      this.color = GenColor.primaryColor,
      this.child,
      this.isBorder = false});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        width: width,
        height: height,
        padding: padding,
        decoration: isBorder
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(radius),
                border: Border.all(color: color))
            : BoxDecoration(
                borderRadius: BorderRadius.circular(radius), color: color),
        child: child != null ?
            child
            : Center(
          child: GenText(
            text,
            style: TextStyle(
                fontWeight: fontWeight, fontSize: fontSize, color: fontColor),
          ),
        ),
      ),
    );
  }
}
