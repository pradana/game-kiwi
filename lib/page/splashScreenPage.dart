import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreenPage>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    // TODO: implement initStat
    controller.forward();

    startSplashScreen();
  }

  @override
  void dispose() {
    super.dispose();
  }

  startSplashScreen() async {

    var token = await getPrefferenceToken();

    if(token == null){
      var duration = const Duration(milliseconds: 2000);
      return Timer(duration, () {
        Navigator.pushReplacementNamed(
            context, "loginPage");
      });
    }else{
      var duration = const Duration(milliseconds: 2000);
      return Timer(duration, () {
        Navigator.pushReplacementNamed(
            context, "/");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: FadeTransition(
        opacity: animation,
        child: Center(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/splash.png"), fit: BoxFit.cover)),
          ),
        ),
      ),
    ));
  }
}
