import 'package:flutter/material.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class LeaderBoard extends StatefulWidget {
  @override
  _LeaderBoardState createState() => _LeaderBoardState();
}

class _LeaderBoardState extends State<LeaderBoard> {
  var bloc;
  var rank = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getHomeData();
    bloc.leaderboard.getLeaderboard();

    return GenPage(
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(50),
            bottom: ScreenUtil().setHeight(50)),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              children: [
                StreamBuilder(
                    stream: bloc.dashboard.avatar,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: GenColor.shimmer),
                          margin: EdgeInsets.only(left: 30),
                          width: ScreenUtil().setWidth(50),
                          height: ScreenUtil().setHeight(50),
                        );
                      }

                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white),
                          margin: EdgeInsets.only(left: ScreenUtil().setSp(30)),
                          width: ScreenUtil().setWidth(50),
                          height: ScreenUtil().setHeight(50),
                          child: ClipOval(
                            child: snapshot.data != null
                                ? Image.network(
                                    snapshot.data,
                                    fit: BoxFit.cover,
                                  )
                                : Image.asset("assets/images/logo.png"),
                          ));
                    }),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          left: ScreenUtil().setSp(10),
                        ),
                        child: StreamBuilder(
                            stream: bloc.dashboard.nickName,
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) {
                                return Container(
                                  width: ScreenUtil().setWidth(100),
                                  height: ScreenUtil().setHeight(30),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: GenColor.shimmer,
                                  ),
                                );
                              }

                              return GenText(
                                snapshot.data ?? "No Name",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              );
                            }),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(5),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: ScreenUtil().setSp(10)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Image.asset(
                              "assets/images/money.png",
                              width: ScreenUtil().setWidth(25),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(5),
                            ),
                            GenText("0",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            SizedBox(
                              width: ScreenUtil().setWidth(15),
                            ),
                            Image.asset(
                              "assets/images/stars.png",
                              width: ScreenUtil().setWidth(25),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(5),
                            ),
                            StreamBuilder(
                                stream: bloc.dashboard.report,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      height: ScreenUtil().setHeight(30),
                                      width: ScreenUtil().setWidth(100),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: GenColor.shimmer,
                                      ),
                                    );
                                  }
                                  return GenText(
                                      snapshot.data["level"] != null
                                          ? "level " + snapshot.data["level"]
                                          : "Level -",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ));
                                }),
                            SizedBox(
                              width: ScreenUtil().setWidth(15),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Expanded(
              child: StreamBuilder(
                  stream: bloc.leaderboard.leaderboard,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    return Column(
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setHeight(20)),
                          height: ScreenUtil().setHeight(150),
                          child: Image.asset("assets/images/thropy.png"),
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(
                                children: snapshot.data.map<Widget>((e) {
                              rank++;

                              return e["isMe"] == "1"
                                  ? Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal:
                                              ScreenUtil().setWidth(20)),
                                      margin: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(20),
                                          vertical: ScreenUtil().setHeight(5)),
                                      height: ScreenUtil().setHeight(70),
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          border: Border.all(
                                              color: GenColor.primaryColor,
                                              width: 2)),
                                      child: Row(
                                        children: [
                                          GenText(
                                            "$rank ",
                                            style: TextStyle(
                                                color: GenColor.primaryColor),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          Container(
                                            height: 40,
                                            width: 40,
                                            child: ClipOval(
                                              child: e["avatar"] != null
                                                  ? Image.network(
                                                      e["avatar"],
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      "assets/images/logo.png"),
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          Expanded(
                                              child: GenText(
                                            e["username"],
                                            style: TextStyle(
                                                color: GenColor.primaryColor,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          )),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          GenText(
                                            e["score"],
                                            style: TextStyle(
                                                color: GenColor.primaryColor),
                                          )
                                        ],
                                      ),
                                    )
                                  : Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal:
                                              ScreenUtil().setWidth(20)),
                                      margin: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(20),
                                          vertical: ScreenUtil().setHeight(5)),
                                      height: ScreenUtil().setHeight(70),
                                      decoration: BoxDecoration(
                                          color: GenColor.primaryColor,
                                          border: Border.all(
                                              color: Colors.black, width: 2)),
                                      child: Row(
                                        children: [
                                          GenText(
                                            "$rank ",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          Container(
                                            height: 40,
                                            width: 40,
                                            child: ClipOval(
                                              child: e["avatar"] != null
                                                  ? Image.network(
                                                      e["avatar"],
                                                      fit: BoxFit.cover,
                                                    )
                                                  : Image.asset(
                                                      "assets/images/logo.png"),
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          Expanded(
                                              child: GenText(
                                            e["username"],
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          )),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                          GenText(
                                            e["score"],
                                            style:
                                                TextStyle(color: Colors.black),
                                          )
                                        ],
                                      ),
                                    );
                            }).toList()),
                          ),
                        ),
                      ],
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
