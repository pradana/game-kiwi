import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/button/genButton.dart';
import 'package:kiwi_game/element/component/button/genButtonLeft.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genDimen.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:provider/provider.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

ProgressDialog pr;

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  var bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getHomeData();
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height, allowFontScaling: true);

    return GenPage(
      statusBarColor: Colors.transparent,
      body: Container(
        width: size.width,
        height: size.height,
        padding: EdgeInsets.only(top: ScreenUtil().setHeight(80)),
        decoration: BoxDecoration(
            color: GenColor.backGroundColor,
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: <Widget>[
            StreamBuilder(
                stream: bloc.dashboard.avatar,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: GenColor.shimmer),
                      margin: EdgeInsets.only(left: 30),
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setHeight(100),
                    );
                  }

                  return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white),
                      margin: EdgeInsets.only(left: 30),
                      width: ScreenUtil().setWidth(100),
                      height: ScreenUtil().setWidth(100),
                      child: ClipOval(
                        child: snapshot.data != null
                            ? Image.network(
                                snapshot.data,
                                fit: BoxFit.cover,
                              )
                            : Image.asset("assets/images/logo.png"),
                      ));
                }),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30),
              child: StreamBuilder(
                  stream: bloc.dashboard.nickName,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                        height: ScreenUtil().setHeight(30),
                        width: ScreenUtil().setWidth(100),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: GenColor.shimmer,
                        ),
                      );
                    }
                    return GenText(
                      snapshot.data ?? "No Name",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil().setSp(16),
                          fontWeight: FontWeight.bold),
                    );
                  }),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    "assets/images/money.png",
                    width: ScreenUtil().setWidth(25),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  GenText("-",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      )),
                  SizedBox(
                    width: 15,
                  ),
                  Image.asset(
                    "assets/images/stars.png",
                    width: ScreenUtil().setWidth(25),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  StreamBuilder(
                      stream: bloc.dashboard.report,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Container(
                            height: ScreenUtil().setHeight(30),
                            width: ScreenUtil().setWidth(100),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: GenColor.shimmer,
                            ),
                          );
                        }
                        return GenText(
                            snapshot.data["level"] != null
                                ? "level " + snapshot.data["level"]
                                : "Level -",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ));
                      }),
                  SizedBox(
                    width: 15,
                  ),
                ],
              ),
            ),
            // SizedBox(
            //   height: ScreenUtil().setHeight(50),
            // ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GenButtonLeft(
                        "Quizes",
                        radius: 50,
                        color: GenColor.accentColor,
                        width: ScreenUtil().setWidth(300),
                        height: ScreenUtil().setHeight(50),
                        fontSize: ScreenUtil().setSp(16),
                        onPressed: () {
                          Navigator.pushNamed(context, "chooseLevelPage");
                        },
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(30),
                      ),
                      GenButtonLeft(
                        "Leaderboard",
                        radius: 50,
                        width: ScreenUtil().setWidth(290),
                        height: ScreenUtil().setHeight(50),
                        fontSize: ScreenUtil().setSp(16),
                        fontColor: Colors.black,
                        onPressed: () {
                          Navigator.pushNamed(context, "leaderBoard");
                        },
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(30),
                      ),
                      GenButtonLeft(
                        "Notification",
                        radius: 50,
                        width: ScreenUtil().setWidth(290),
                        height: ScreenUtil().setHeight(50),
                        fontSize: ScreenUtil().setSp(16),
                        fontColor: Colors.black,
                        onPressed: () {
                          Navigator.pushNamed(context, "notification");
                        },
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(30),
                      ),
                      GenButtonLeft(
                        "Coins",
                        radius: 50,
                        height: ScreenUtil().setHeight(50),
                        width: ScreenUtil().setWidth(290),
                        fontSize: ScreenUtil().setSp(16),
                        fontColor: Colors.black,
                        onPressed: () {
                          Navigator.pushNamed(context, "coin");
                        },
                      ),
                      SizedBox(
                        height: ScreenUtil().setHeight(50),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // Spacer(
            //   flex: 1,
            // ),
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: GenDimen.sidePadding,
                  vertical: ScreenUtil().setHeight(30)),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: GenButton(
                    "Settings",
                    radius: 50,
                    fontSize: 16,
                    height: ScreenUtil().setHeight(50),
                    fontColor: Colors.black,
                    onPressed: () {
                      _showMyDialog();
                    },
                  )),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: GenButton(
                    "Logout",
                    radius: 50,
                    height: ScreenUtil().setHeight(50),
                    fontSize: 16,
                    fontColor: GenColor.primaryColor,
                    isBorder: true,
                    onPressed: () {
                      signOut();
                    },
                  )),
                ],
              ),
              //   )
              // ],
            ),
          ],
        ),
      ),
    );
  }

  Future<FirebaseUser> signOut() async {
    print("try signed out");
    pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
    pr.style(message: "Please Wait...");
    pr.show();

    try {
      await FirebaseAuth.instance.signOut().then((value) async {
        await _googleSignIn.signOut().then((e) {
          logout();
          Navigator.pushReplacementNamed(context, "loginPage");
        });
      });
    } on SocketException catch (_) {
      Toast.show("Terjadi kesalahan", context);
      print("no connection");
      pr.hide();
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Setting'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Coming Soon.'),
                Text('Under Construction'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
