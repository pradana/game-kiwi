
import 'package:flutter/material.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  var bloc;
  var rank = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);
    bloc = Provider.of<BaseBloc>(context);
    bloc.dashboard.getHomeData();
    bloc.leaderboard.getLeaderboard();

    return GenPage(
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(50),
            bottom: ScreenUtil().setHeight(50)),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              children: [
                StreamBuilder(
                    stream: bloc.dashboard.avatar,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: GenColor.shimmer),
                          margin: EdgeInsets.only(left: 30),
                          width: ScreenUtil().setWidth(50),
                          height: ScreenUtil().setHeight(50),
                        );
                      }

                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white),
                          margin: EdgeInsets.only(left: ScreenUtil().setSp(30)),
                          width: ScreenUtil().setWidth(50),
                          height: ScreenUtil().setHeight(50),
                          child: ClipOval(
                            child: snapshot.data != null
                                ? Image.network(
                              snapshot.data,
                              fit: BoxFit.cover,
                            )
                                : Image.asset("assets/images/logo.png"),
                          ));
                    }),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          left: ScreenUtil().setSp(10),
                        ),
                        child: StreamBuilder(
                            stream: bloc.dashboard.nickName,
                            builder: (context, snapshot) {
                              if (!snapshot.hasData) {
                                return Container(
                                  width: ScreenUtil().setWidth(100),
                                  height: ScreenUtil().setHeight(30),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: GenColor.shimmer,
                                  ),
                                );
                              }

                              return GenText(
                                snapshot.data ?? "No Name",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              );
                            }),
                      ),
                      SizedBox(
                        height: ScreenUtil().setWidth(5),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: ScreenUtil().setSp(10)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Image.asset(
                              "assets/images/money.png",
                              width: ScreenUtil().setWidth(25),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(5),
                            ),
                            GenText("0",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            SizedBox(
                              width: ScreenUtil().setWidth(15),
                            ),
                            Image.asset(
                              "assets/images/stars.png",
                              width: ScreenUtil().setWidth(25),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(5),
                            ),
                            StreamBuilder(
                                stream: bloc.dashboard.report,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      height: ScreenUtil().setHeight(30),
                                      width: ScreenUtil().setWidth(100),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: GenColor.shimmer,
                                      ),
                                    );
                                  }
                                  return GenText(
                                      snapshot.data["level"] != null
                                          ? "level " + snapshot.data["level"]
                                          : "Level -",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ));
                                }),
                            SizedBox(
                              width: ScreenUtil().setWidth(15),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30.w),
                    width: double.infinity,
                    height: 50,
                    color: GenColor.primaryColor,
                    child: Center(
                      child: GenText(
                        "Notification", style: TextStyle(fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(20)),
                      ),
                    ),
                  ), SizedBox(height: ScreenUtil().setHeight(10),),Container(
                    margin: EdgeInsets.symmetric(horizontal: 30.w),
                    width: double.infinity,
                    color: Colors.white,
                    padding: EdgeInsets.all(16.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GenText("New Release 1.0.6", style: TextStyle(fontSize: ScreenUtil().setSp(20)),),
                        SizedBox(height: ScreenUtil().setHeight(10),),
                        GenText("- Leader Board", style: TextStyle(fontSize: ScreenUtil().setSp(14)),),
                        GenText("- Unlock Quiz", style: TextStyle(fontSize: ScreenUtil().setSp(14)),),
                        GenText("- Premium User", style: TextStyle(fontSize: ScreenUtil().setSp(14)),),

                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
