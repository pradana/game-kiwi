
import 'package:flutter/material.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';

class Coin extends StatefulWidget {
  @override
  _CoinState createState() => _CoinState();
}

class _CoinState extends State<Coin> {
  @override
  Widget build(BuildContext context) {
    return GenPage(
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 120, bottom: 50),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          children: <Widget>[
            GenText(
              "COINS",
              style: TextStyle(
                  color: GenColor.primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),

            Expanded(
              child: Container(
                width: 500,
                child: Image.asset("assets/images/comingsoon.png"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
