import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/card/genCardButton.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genDimen.dart';
import 'package:provider/provider.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:toast/toast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'ChoosePackagePage.dart';

class ChooseLevelPage extends StatefulWidget {
  @override
  _ChooseLevelPageState createState() => _ChooseLevelPageState();
}

class _ChooseLevelPageState extends State<ChooseLevelPage> {
  var bloc;

  @override
  Widget build(BuildContext context) {
    bloc = Provider.of<BaseBloc>(context);
    bloc.level.getAllLevel();

    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);

    return StreamBuilder(
        stream: bloc.level.level,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return GenPage(
              body: Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(10)),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/bg.png"),
                        fit: BoxFit.cover)),
                child: Center(child: CircularProgressIndicator()),
              ),
            );
          }

          var levelPLayer = snapshot.data["level"];
          return GenPage(
            statusBarColor: Colors.transparent,
            body: Container(
              width: double.infinity,
              height: size.height,
              padding: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(10)),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/bg.png"),
                      fit: BoxFit.cover)),
              child: SingleChildScrollView(
                child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: GenDimen.sidePadding, vertical: ScreenUtil().setHeight(20)),
                        child: Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                if (Platform.isIOS) {
                                  Navigator.pop(context);
                                }
                              },
                              child: Platform.isIOS
                                  ? Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                    )
                                  : Icon(
                                      Icons.menu,
                                      color: Colors.white,
                                    ),
                            ),
                            Expanded(
                              child: Center(
                                  child: GenText(
                                "Choose Level",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                            StreamBuilder(
                                stream: bloc.dashboard.avatar,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: GenColor.shimmer),
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setSp(38),
                                      ),
                                      width: ScreenUtil().setWidth(50),
                                      height: ScreenUtil().setWidth(50),
                                    );
                                  }

                                  return Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: Colors.white),
                                      margin: EdgeInsets.only(
                                        left: ScreenUtil().setSp(38),
                                      ),
                                      width: ScreenUtil().setWidth(50),
                                      height: ScreenUtil().setWidth(50),
                                      child: ClipOval(
                                        child: snapshot.data != null
                                            ? Image.network(
                                                snapshot.data,
                                                fit: BoxFit.cover,
                                              )
                                            : Image.asset(
                                                "assets/images/logo.png"),
                                      ));
                                }),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 70,
                      ),
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: GenDimen.sidePadding),
                          child: Wrap(
                              children:
                                  snapshot.data["dataLevel"].map<Widget>((e) {
                            var level = e["level"];
                            print("level player $levelPLayer , level $level");
                            return levelPLayer + 1 > level
                                ? GenCardButton(
                                    width: ScreenUtil().setWidth(70),
                                    height: ScreenUtil().setWidth(70),
                                    child: Icon(
                                      FeatherIcons.play,
                                      color: GenColor.accentColor,
                                      size: 30,
                                    ),
                                    label: "level " + e["level"].toString(),
                                    fontColor: GenColor.accentColor,
                                    radius: 20,
                                    margin:
                                        EdgeInsets.all(ScreenUtil().setSp(10)),
                                    ontap: () {
                                      Navigator.pushNamed(context,
                                          ChoosePackagePageState.routeName,
                                          arguments: ChoosePackagePage(
                                            level: e['level'],
                                          ));
                                    },
                                  )
                                : GenCardButton(
                                    width: ScreenUtil().setWidth(70),
                                    height: ScreenUtil().setWidth(70),
                                    child: Icon(
                                      FeatherIcons.lock,
                                      color: GenColor.primaryColor,
                                      size: 30,
                                    ),
                                    label: "level " + e["level"].toString(),
                                    fontColor: GenColor.primaryColor,
                                    radius: 20,
                                    backGroundColor: GenColor.accentColor,
                                    margin:
                                        EdgeInsets.all(ScreenUtil().setSp(10)),
                                    ontap: () {
                                      Toast.show("LOCKED", context);
                                    },
                                  );
                          }).toList())),
                    ],
                  ),

              ),
            ),
          );
        });
  }
}
