
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/button/genButton.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genDimen.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'finishQuiz.dart';

class PlayQuiz extends StatefulWidget {
  final int idPackage;
  final double yourScore;
  PlayQuiz({this.idPackage, this.yourScore});

  @override
  PlayQuizState createState() => PlayQuizState();
}

class PlayQuizState extends State<PlayQuiz> {
  static const routeName = 'playQuiz';
  var bloc, idPackage, yourscore;
  final req = new GenRequest();
  var noSoal = 0;
  var totalSoal = 1;
  var pageController;
  bool loading = false;
  int trueAnswers = 0;

  @override
  void initState() {
    // TODO: implement initState
    pageController =
        PageController(initialPage: 0, viewportFraction: 1, keepPage: true);
    noSoal = 0;
    totalSoal = 1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final PlayQuiz args = ModalRoute.of(context).settings.arguments;
    idPackage = args.idPackage;
    yourscore = args.yourScore;
    bloc = Provider.of<BaseBloc>(context);
    bloc.play.getPlayQuiz(idPackage);
    bloc.dashboard.getHomeData();
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);

    return GenPage(
        statusBarColor: Colors.transparent,
        body: Container(
          width: size.width,
          height: size.height,
          padding: EdgeInsets.only(top: ScreenUtil().setSp(50)),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg.png"),
                  fit: BoxFit.cover)),
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setSp(10)),
                child: Row(
                  children: <Widget>[
                    StreamBuilder(
                        stream: bloc.dashboard.avatar,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: GenColor.shimmer),
                              margin: EdgeInsets.only(left: 30),
                              width: ScreenUtil().setWidth(50),
                              height: ScreenUtil().setHeight(50),
                            );
                          }

                          return Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white),
                              margin:
                              EdgeInsets.only(left: ScreenUtil().setSp(30)),
                              width: ScreenUtil().setWidth(50),
                              height: ScreenUtil().setHeight(50),
                              child: ClipOval(
                                child: snapshot.data != null
                                    ? Image.network(
                                  snapshot.data,
                                  fit: BoxFit.cover,
                                )
                                    : Image.asset("assets/images/logo.png"),
                              ));
                        }),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(
                              left: ScreenUtil().setSp(10),
                            ),
                            child: StreamBuilder(
                                stream: bloc.dashboard.nickName,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      width: ScreenUtil().setWidth(100),
                                      height: ScreenUtil().setHeight(30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: GenColor.shimmer,
                                      ),
                                    );
                                  }
                                  return GenText(
                                    snapshot.data ?? "No Name",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  );
                                }),
                          ),
                          SizedBox(
                            height: ScreenUtil().setWidth(5),
                          ),
                          Padding(
                            padding:
                            EdgeInsets.only(left: ScreenUtil().setSp(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset("assets/images/money.png", width: ScreenUtil().setWidth(25),),
                                SizedBox(
                                  width: ScreenUtil().setWidth(5),
                                ),
                                GenText("-",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                    )),
                                SizedBox(
                                  width: ScreenUtil().setWidth(15),
                                ),
                                Image.asset("assets/images/stars.png", width: ScreenUtil().setWidth(25),),
                                SizedBox(
                                  width: ScreenUtil().setWidth(5),
                                ),
                                StreamBuilder(
                                    stream: bloc.dashboard.report,
                                    builder: (context, snapshot) {
                                      if (!snapshot.hasData) {
                                        return Container(
                                          height: ScreenUtil().setHeight(30),
                                          width: ScreenUtil().setWidth(100),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(10),
                                            color: GenColor.shimmer,
                                          ),
                                        );
                                      }
                                      return GenText(
                                          snapshot.data["level"] != null
                                              ? "level " +
                                              snapshot.data["level"]
                                              : "Level -",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ));
                                    }),
                                SizedBox(
                                  width: ScreenUtil().setWidth(15),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    StreamBuilder(
                        stream: bloc.play.playQuizBloc,
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container();
                          }

                          totalSoal = snapshot.data.length;
                          return GenText(
                            (noSoal + 1).toString() +
                                "/" +
                                totalSoal.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          );
                        })
                  ],
                ),
              ),
              StreamBuilder(
                  stream: bloc.play.playQuizBloc,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(50),
                        ),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/bg.png"),
                                fit: BoxFit.cover)),
                        child: Center(child: CircularProgressIndicator()),
                      );
                    }

                    return AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: loading ? 0 : size.height  - 150,
                        width: size.width,
                        child: PageView(
                            controller: pageController,
                            physics: new NeverScrollableScrollPhysics(),
                            children: snapshot.data.map<Widget>((e) {
                              return SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    SizedBox(
                                      height: ScreenUtil().setHeight(30),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal:
                                          ScreenUtil().setWidth(30)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.white,
                                      ),
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsets.all(8),
                                            height: ScreenUtil().setHeight(250),
                                            child: ClipRRect(
                                                borderRadius:
                                                BorderRadius.circular(15),
                                                child: e["assets"].length == 0
                                                    ? Image.asset(
                                                    "assets/images/logo.png")
                                                    : Image.network(
                                                  ip +
                                                      e["assets"][0]
                                                      ["url"],
                                                  fit: BoxFit.cover,
                                                )),
                                          ),
                                          Container(
                                              margin: EdgeInsets.all(14),
                                              child: GenText(
                                                e["question"],
                                                style: TextStyle(fontSize: 18),
                                              )),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setHeight(30),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: GenDimen.sidePadding),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: GenButton(
                                                e["option"][0]["option"],
                                                radius: 50,
                                                fontSize: 16,
                                                fontColor: Colors.black,
                                                onPressed: () {
                                                  setState(() {
                                                    gotoPage(e["id"], 0,
                                                        e["option"][0]["id"]);
                                                  });
                                                },
                                              )),
                                          SizedBox(
                                            width: ScreenUtil().setWidth(10),
                                          ),
                                          Expanded(
                                            child: GenButton(
                                              e["option"][1]["option"],
                                              radius: 50,
                                              fontSize: 16,
                                              fontColor: Colors.black,
                                              onPressed: () {
                                                setState(() {
                                                  gotoPage(e["id"], 1,
                                                      e["option"][0]["id"]);
                                                });
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setHeight(10),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: GenDimen.sidePadding),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                              child: GenButton(
                                                e["option"][2]["option"],
                                                radius: 50,
                                                fontSize: 16,
                                                fontColor: Colors.black,
                                                onPressed: () {
                                                  setState(() {
                                                    gotoPage(e["id"], 2,
                                                        e["option"][0]["id"]);
                                                  });
                                                },
                                              )),
                                          SizedBox(
                                            width: ScreenUtil().setWidth(10),
                                          ),
                                          Expanded(
                                              child: GenButton(
                                                e["option"][3]["option"],
                                                radius: 50,
                                                fontSize: 16,
                                                fontColor: Colors.black,
                                                onPressed: () {
                                                  setState(() {
                                                    gotoPage(e["id"], 3,
                                                        e["option"][0]["id"]);
                                                  });
                                                },
                                              )),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList()));
                  })
            ],
          ),
        ));
  }

  void gotoPage(quizid, answerIndex, answerId) async {
    setState(() {
      loading = true;
    });

    dynamic response = await req.postApi("quiz/submit",
        {"quizId": quizid, "answerIndex": answerIndex, "answerId": answerId});

    if (response["code"] == 200) if (noSoal < totalSoal - 1) {
      print("RESPONSE JAWAB --------------------");
      print(response);
      noSoal = noSoal + 1;
      pageController.jumpToPage(noSoal);


      setState(() {
        loading = false;
      });
    } else {
      Navigator.pushReplacementNamed(context, FinishQuizState.routeName,
          arguments: FinishQuiz(idPackage: idPackage, scoreAwal: yourscore));

      setState(() {
        loading = false;
      });
    }
  }
}
