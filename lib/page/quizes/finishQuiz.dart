import 'dart:math';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/button/genButton.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genDimen.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FinishQuiz extends StatefulWidget {

  final int idPackage;
  final double scoreAwal;
  FinishQuiz({this.idPackage, this.scoreAwal});

  @override
  FinishQuizState createState() => FinishQuizState();
}

class FinishQuizState extends State<FinishQuiz> {
  var bloc, idPackage, scoreAwal;
  static const routeName = 'finishQuiz';

  @override
  Widget build(BuildContext context) {

    final FinishQuiz args = ModalRoute
        .of(context)
        .settings
        .arguments;
    idPackage = args.idPackage;
    scoreAwal = args.scoreAwal;
    bloc = Provider.of<BaseBloc>(context);
    bloc.finish.getFinishPage(idPackage);
    bloc.dashboard.getHomeData();

    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);

    return GenPage(
      statusBarColor: Colors.transparent,
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 50),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: GenDimen.sidePadding),
                        child: Row(
                          children: <Widget>[
                            StreamBuilder(
                                stream: bloc.dashboard.avatar,
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(50),
                                          color: GenColor.shimmer),
                                      margin: EdgeInsets.only(left: 30),
                                      width: 50,
                                      height: 50,
                                    );
                                  }

                                  return Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(50),
                                          color: Colors.white),
                                      margin: EdgeInsets.only(left: 30),
                                      width: 50,
                                      height: 50,
                                      child: ClipOval(
                                        child: snapshot.data != null
                                            ? Image.network(
                                          snapshot.data,
                                          fit: BoxFit.cover,
                                        )
                                            : Image.asset("assets/images/logo.png"),
                                      ));
                                }),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: StreamBuilder(
                                        stream: bloc.dashboard.nickName,
                                        builder: (context, snapshot) {
                                          if (!snapshot.hasData) {
                                            return Container(
                                              height: 30,
                                              width: 100,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                                color: GenColor.shimmer,
                                              ),
                                            );
                                          }
                                          return GenText(
                                            snapshot.data ?? "No Name",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Image.asset(
                                          "assets/images/money.png",
                                          width: ScreenUtil().setWidth(25),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        GenText("-",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                            )),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Image.asset(
                                          "assets/images/stars.png",
                                          width: ScreenUtil().setWidth(25),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        StreamBuilder(
                                            stream: bloc.dashboard.report,
                                            builder: (context, snapshot) {
                                              if (!snapshot.hasData) {
                                                return Container(
                                                  height: 30,
                                                  width: 100,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                    BorderRadius.circular(10),
                                                    color: GenColor.shimmer,
                                                  ),
                                                );
                                              }
                                              return GenText(
                                                  snapshot.data["level"] != null
                                                      ? "level " +
                                                      snapshot.data["level"]
                                                      : "Level -",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                  ));
                                            }),
                                        SizedBox(
                                          width: 15,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: Center(
                child: Container(
                  height: 450,
                  width: double.infinity,
                  padding: EdgeInsets.all(GenDimen.sidePadding),
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.white,
                  ),
                  child: StreamBuilder(
                    stream: bloc.finish.finish,
                    builder: (context, snapshot) {
                      if(!snapshot.hasData){
                        return Container(child: Center(child: SizedBox(height: 50, width: 50, child: CircularProgressIndicator())),);
                      }

                      var hasilExp = double.parse(snapshot.data[0]["exp"]) - scoreAwal;
                      var maxXP = double.parse(snapshot.data[0]["maxExp"]);
                      var star;
                      double score = double.parse(snapshot.data[0]["exp"]);

                      print("score exp ----------");
                      print(score);

                      print("max exp ----------");
                      print(maxXP);

                      if(score == maxXP){
                        star = 3;
                      }else if(score < (2 * maxXP/3)){
                        star = 2;
                      }else{
                        star = 1;
                      }

                      print(star);
                      int fac = pow(10, 2);
                      score = (score * fac).round() / fac;
                      hasilExp = (hasilExp * fac).round() / fac;
                      var ismin = double.parse(snapshot.data[0]["exp"]) < scoreAwal;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[
                           GenText("Complete", style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),),
                           SizedBox(height: 30,),
                           star == 3 ? Row(mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             Icon(Icons.star, color: GenColor.starColor, size: 50,),
                             SizedBox(width: 10,),
                             Icon(Icons.star, color: GenColor.starColor, size: 50),
                             SizedBox(width: 10,),
                             Icon(Icons.star, color: GenColor.starColor, size: 50),
                             SizedBox(width: 10,),
                           ],) : star == 2 ? Row(mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Icon(Icons.star, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                               Icon(Icons.star, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                               Icon(Icons.star_border, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                             ],) : Row(mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Icon(Icons.star, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                               Icon(Icons.star_border, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                               Icon(Icons.star_border, color: GenColor.starColor, size: 50),
                               SizedBox(width: 10,),
                             ],),
                           Expanded(
                             child: SingleChildScrollView(
                               child: Column(
                                 mainAxisAlignment: MainAxisAlignment.center,
                                 children: <Widget>[
                                   SizedBox(height: ScreenUtil().setHeight(20),),
                                   GenText("Your Score", style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),),
                                   GenText(score.toString(), style: TextStyle(fontSize: 56, fontWeight: FontWeight.bold, color: GenColor.accentColor),),
                                   SizedBox(height: 30,),
                                   ismin ? GenText("you lose exp ($hasilExp)", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: GenColor.accentColor),) : GenText("you earned exp($hasilExp)", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.green),) ,
                                 ],
                               ),
                             ),
                           ),
                          GenButton("Main Menu", color: GenColor.accentColor, fontWeight: FontWeight.bold, onPressed: (){Navigator.pushReplacementNamed(context, "/");},)
                         ],
                      );
                    }
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
