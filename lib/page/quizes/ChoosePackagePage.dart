import 'dart:io';


import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:kiwi_game/blocs/baseBloc.dart';
import 'package:kiwi_game/element/component/button/genButton.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genDimen.dart';
import 'package:kiwi_game/page/quizes/playQuiz.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChoosePackagePage extends StatefulWidget {
  final int level;
  ChoosePackagePage({this.level});

  @override
  ChoosePackagePageState createState() => ChoosePackagePageState();
}

class ChoosePackagePageState extends State<ChoosePackagePage> {
  static const routeName = 'choosePackage';
  var bloc, level;

  @override
  Widget build(BuildContext context) {
    final ChoosePackagePage args = ModalRoute.of(context).settings.arguments;
    level = args.level;
    bloc = Provider.of<BaseBloc>(context);
    bloc.package.getPackageQuiz(level);

    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);

    return GenPage(
      statusBarColor: Colors.transparent,
      body: Container(
        width: double.infinity,
        height: size.height,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setSp(10)),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:
                   EdgeInsets.symmetric(horizontal: GenDimen.sidePadding, vertical: ScreenUtil().setHeight(20)),
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      if (Platform.isIOS) {
                        Navigator.pop(context);
                      }
                    },
                    child: Platform.isIOS
                        ? Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          )
                        : Icon(
                            Icons.menu,
                            color: Colors.white,
                          ),
                  ),
                  Expanded(
                    child: Center(
                        child: GenText(
                      "Quizes",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )),
                  ),
                  StreamBuilder(
                      stream: bloc.dashboard.avatar,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: GenColor.shimmer),
                            margin: EdgeInsets.only(left: 30),
                            width: 50.w,
                            height: 50.w,
                          );
                        }

                        return Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.white),
                            margin: EdgeInsets.only(left: 30),
                            width: 50.w,
                            height: 50.w,
                            child: ClipOval(
                              child: snapshot.data != null
                                  ? Image.network(
                                      snapshot.data,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.asset("assets/images/logo.png"),
                            ));
                      }),
                ],
              ),
            ),
            SizedBox(
              height: 30.h,
            ),
            StreamBuilder(
                stream: bloc.package.packageQuiz,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setSp(10)),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/bg.png"),
                              fit: BoxFit.cover)),
                      child: Center(child: CircularProgressIndicator()),
                    );
                  }

                  return snapshot.data.length == 0
                      ? Expanded(
                          child: Container(
                            margin:
                                EdgeInsets.only(bottom: ScreenUtil().setSp(45)),
                            child: Center(
                              child: GenText(
                                "OOPS, THERE IS NO QUIZ HERE, TRY ANOTHER LEVEL",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setSp(15)),
                          child: Container(
                            child: CarouselSlider(
                              options: CarouselOptions(
                                height: 500.h,
                                enableInfiniteScroll: false,
                                enlargeCenterPage: true,
                              ),
                              items: snapshot.data.map<Widget>((i) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    var images = ip + "/" + i["coverImg"];
                                    print("yourscore");
                                    print(i);
                                    return Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        // margin: EdgeInsets.symmetric(
                                        //     horizontal: 5.0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        child: Container(
                                          padding: const EdgeInsets.all(12),
                                          child: Column(
                                            children: <Widget>[
                                              Align(alignment: Alignment.centerRight, child: GenText(i["score"] == null ? "Your Score : 0/"+i["exp"].toString(): "Your Score : "+i["score"].toString()+"/"+i["exp"].toString(), style: TextStyle(fontWeight: FontWeight.bold),), ),
                                              SizedBox(
                                                height: 30,
                                              ),
                                              Container(
                                                height: 300.h,
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                    child: Image.network(
                                                      images,
                                                      fit: BoxFit.cover,
                                                    )),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Flexible(
                                                        flex: 1,
                                                        child: GenText(
                                                          i["packageName"],
                                                          style: TextStyle(
                                                              fontSize: 26),
                                                        )),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    i["status"] == "unlocked" ? Container() : Icon(
                                                      Icons.lock_outline,
                                                      color: Colors.grey),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    i["status"] == "unlocked" ? Flexible(
                                                        flex: 1,
                                                        child: GenButton(
                                                          "GO",
                                                          height: 50.h,
                                                          width: 150.w,
                                                          radius: 50,
                                                          fontColor:
                                                              Colors.white,
                                                          fontSize: 28,
                                                          color: GenColor
                                                              .accentColor,
                                                          onPressed: () {
                                                            Navigator.pushNamed(
                                                                context,
                                                                PlayQuizState
                                                                    .routeName,
                                                                arguments:

                                                                    PlayQuiz(
                                                                  idPackage:
                                                                      i['id'],
                                                                  yourScore:
                                                                  double.parse(i['score'] ??
                                                                          "0"),
                                                                ));
                                                          },
                                                        )) : Flexible(
                                                        flex: 1,
                                                        child: GenButton(
                                                          "GO",
                                                          child: Row(
                                                            children: [
                                                              Image.asset(
                                                                "assets/images/money.png",
                                                                width: ScreenUtil().setWidth(25),
                                                              ),SizedBox(width: 10.w,),
                                                              GenText("UNLOCK", style: TextStyle(color: GenColor.starColor, fontWeight: FontWeight.bold),)
                                                            ],
                                                          ),
                                                          height: 50.h,
                                                          width: 150.w,
                                                          radius: 50,
                                                          fontColor:
                                                          Colors.white,
                                                          fontSize: 28,
                                                          color: GenColor
                                                              .accentColor,
                                                          onPressed: () {
                                                            Navigator.pushNamed(
                                                                context,
                                                                PlayQuizState
                                                                    .routeName,
                                                                arguments:

                                                                PlayQuiz(
                                                                  idPackage:
                                                                  i['id'],
                                                                  yourScore:
                                                                  double.parse(i['score'] ??
                                                                      "0"),
                                                                ));
                                                          },
                                                        )) ,
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ));
                                  },
                                );
                              }).toList(),
                            ),
                          ));
                }),
          ],
        ),
      ),
    );
  }
}
