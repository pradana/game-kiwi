import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kiwi_game/api/request.dart';
import 'package:kiwi_game/element/component/button/genButton.dart';
import 'package:kiwi_game/element/component/page/genPage.dart';
import 'package:kiwi_game/element/component/text/genText.dart';
import 'package:kiwi_game/element/etc/genColor.dart';
import 'package:kiwi_game/element/etc/genPreferrence.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

ProgressDialog pr;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context, width: size.width, height: size.height);

    return GenPage(
      statusBarColor: Colors.transparent,
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(100)),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bg.png"), fit: BoxFit.cover)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(150),
              child: Image.asset("assets/images/logo.png"),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(50),
            ),
            Container(
              width: ScreenUtil().setWidth(220),
              child: Image.asset("assets/images/whosmarter.png"),
            ),
            Spacer(
              flex: 1,
            ),
            Column(
              children: <Widget>[
                GenText(
                  "Let's Play",
                  style: TextStyle(color: Colors.white, fontSize: ScreenUtil().setSp(16)),
                ),
                SizedBox(
                  height: 5,
                ),
                GenButton(
                  "Start Playing",
                  color: GenColor.accentColor,
                  fontColor: Colors.white,
                  width: ScreenUtil().setWidth(220),
                  height: ScreenUtil().setHeight(50),
                  fontSize: ScreenUtil().setSp(18),
                  radius: 50,
                  onPressed: () {
                    _handleSignIn()
                        .then((FirebaseUser user) => print(user))
                        .catchError((e) => print(e));
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<FirebaseUser> _handleSignIn() async {
    print("try signed in");

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );

        pr = new ProgressDialog(context, type: ProgressDialogType.Normal);
        pr.style(message: "Please Wait...");
        pr.show();

        final FirebaseUser user =
            (await _auth.signInWithCredential(credential)).user;
        IdTokenResult fbr = await user.getIdToken(refresh: true);
        String token = fbr.token;
        String avatar = googleUser.photoUrl;
        String nama = googleUser.displayName;

        var response = await postLogin(token);
        print("response $response");
//        launchWhatsApp(phone: "628975050520", message: "token: $token");
        if (response != null) {
          if (response["code"] == 200) {
            setPrefferenceToken(token);
            setPrefferenceAvatar(avatar);
            await setPrefferenceNama(nama);
            Navigator.pushReplacementNamed(context, "/");
          } else {
            pr.hide();
            Toast.show("login failed, please try again later", context);
            await FirebaseAuth.instance.signOut();
            await _googleSignIn.signOut();
          }
        } else {
          pr.hide();
          Toast.show("login failed, please try again later", context);
          await FirebaseAuth.instance.signOut();
          await _googleSignIn.signOut();
        }

//        launchWhatsApp(phone: "628975050520", message: token);
        return user;
      }
    } on SocketException catch (_) {
      Toast.show("Terjadi kesalahan", context);
      print("no connection");
    }
  }
}
